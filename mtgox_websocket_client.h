#pragma once


class MtgoxWebsocketClient : public MtgoxClient
{
    typedef websocketpp::client<websocketpp::config::asio_client> websocket_client_t;
    typedef websocketpp::config::asio_client::message_type::ptr websocket_message_ptr_t;

    websocket_client_t _client;

    boost::thread _websocket_loop;

    std::deque<FeedTick> _ticks;
    std::mutex _ticks_sync;
    
public:
    MtgoxWebsocketClient()
    {
        _client.init_asio();

        _client.set_pong_timeout_handler(websocketpp::lib::bind(&MtgoxWebsocketClient::OnWebsocketClose, this));
        _client.set_interrupt_handler(websocketpp::lib::bind(&MtgoxWebsocketClient::OnWebsocketClose, this));
        _client.set_close_handler(websocketpp::lib::bind(&MtgoxWebsocketClient::OnWebsocketClose, this));
        _client.set_fail_handler(websocketpp::lib::bind(&MtgoxWebsocketClient::OnWebsocketClose, this));

        _client.set_message_handler(
            websocketpp::lib::bind(
                &MtgoxWebsocketClient::OnWebsocketMessage, 
                this,
                websocketpp::lib::placeholders::_1,
                websocketpp::lib::placeholders::_2
                )
            );

        _websocket_loop = boost::thread(
            boost::bind(&MtgoxWebsocketClient::WebsocketLoop, this)
            );
    }

    virtual ~MtgoxWebsocketClient()
    {
        Done(true);

        if (!_client.stopped())
        {
            _client.stop();
        }

        if (_websocket_loop.joinable())
        {
            _websocket_loop.join();
        }
    }

private:
    virtual void PopTicks(FeedData * data)
    {
        std::lock_guard<std::mutex> lock(_ticks_sync);

        data->ticks_count = 0;

        while (!_ticks.empty() && data->ticks_count < COUNTOF(data->ticks))
        {
            data->ticks[data->ticks_count++] = _ticks.front();
            _ticks.pop_front();
        }
    }

    void PushTick(const std::string & symbol, double bid, double ask)
    {
        std::lock_guard<std::mutex> lock(_ticks_sync);

        FeedTick tick = {0};

        if (PrepareTick(tick, symbol, bid, ask))
        {
            _ticks.push_back(tick);
        }
    }

    void WebsocketLoop()
    {
        websocketpp::lib::error_code error_code;
        websocket_client_t::connection_ptr con = _client.get_connection(
            "ws://websocket.mtgox.com:80/mtgox?Currency=" + CURRENCIES, 
            error_code
            );

        if (error_code)
        {
            Done(true);
            return;
        }

        con->replace_header("Origin", "http://www.latypoff.com");
        
        _client.connect(con);

        while (!Done())
        {
            _client.run_one();
        }
    }

    void OnWebsocketClose()
    {
        Done(true);

        if (!_client.stopped())
        {
            _client.stop();
        }
    }

    void OnWebsocketMessage(websocketpp::connection_hdl hdl, websocket_message_ptr_t msg)
    {
        try
        {
            ParseMessage(msg->get_payload());
        }
        catch (const std::exception &)
        {

        }
    }

    void ParseMessage(const std::string & message)
    {
        boost::property_tree::ptree message_pt = ConstructPtFromString(message);

        if (message_pt.get<std::string>("private") != "ticker")
        {
            return;
        }

        const auto & channel_name = message_pt.get<std::string>("channel_name");

        const auto dot = channel_name.find(".BTC");

        if (dot == std::string::npos)
        {
            return;
        }

        const auto & symbol = channel_name.substr(dot + 1);

        if (symbol.empty())
        {
            return;
        }

        PushTick(
            symbol, 
            message_pt.get<double>("ticker.buy.value"), 
            message_pt.get<double>("ticker.sell.value")
            );
    }

};
