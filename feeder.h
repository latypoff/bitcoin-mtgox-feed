#pragma once

class Feeder : public CFeedInterface
{
private:

#ifdef MTGOX_HTTP
    std::shared_ptr<MtgoxHTTPClient> _client;
#else
    std::shared_ptr<MtgoxWebsocketClient> _client;
#endif

public:
    virtual int Connect(LPCSTR datafeedSource, LPCSTR login, LPCSTR password)
    {
        try
        {
            #ifdef MTGOX_HTTP
            std::atomic_store(&_client, std::make_shared<MtgoxHTTPClient>());
            #else
            std::atomic_store(&_client, std::make_shared<MtgoxWebsocketClient>());
            #endif
        }
        catch (const std::exception & ex)
        {
            std::cout << "Connect exception: " << ex.what() << std::endl; 
            return FALSE;
        }
        catch (...)
        {
            std::cout << "Connect exception" << std::endl; 
            return FALSE;
        }

        return TRUE;
    }

    virtual void Close()
    {
        try
        {
            #ifdef MTGOX_HTTP
            std::atomic_store(&_client, std::shared_ptr<MtgoxHTTPClient>());
            #else
            std::atomic_store(&_client, std::shared_ptr<MtgoxWebsocketClient>());
            #endif
        }
        catch (const std::exception & ex)
        {
            std::cout << "Close exception: " << ex.what() << std::endl; 
        }
        catch (...)
        {
            std::cout << "Close exception" << std::endl; 
        }
    }

    virtual void SetSymbols(LPCSTR symbols) 
    {

    }

    virtual int Read(FeedData * data) 
    {
        try
        {
            if (!data)
            {
                return FALSE;
            }
        
            auto client = std::atomic_load(&_client);

            if (!client->GetTicks(data))
            {
                return FALSE;
            }

            if (!data->ticks_count)
            {
                boost::this_thread::sleep_for(boost::chrono::milliseconds(50));
            }
        }
        catch (const std::exception & ex)
        {
            std::cout << "Read exception: " << ex.what() << std::endl; 
            return FALSE;
        }
        catch (...)
        {
            std::cout << "Read exception" << std::endl; 
            return FALSE;
        }

        return TRUE;
    }

    virtual int Journal(char * buffer) 
    { 
        return 0; 
    }

};
