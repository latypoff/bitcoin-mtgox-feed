#pragma once

#include "targetver.h"

#include <deque>
#include <atomic>
#include <thread>
#include <mutex>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include <windows.h>

#ifdef MTGOX_HTTP
#include <WinInet.h>
#endif

#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

#include <mt4/FeedInterface.h>

#include "mtgox_client.h"

#ifdef MTGOX_HTTP
#include "mtgox_http_client.h"
#else
#include "mtgox_websocket_client.h"
#endif

#include "feeder.h"
