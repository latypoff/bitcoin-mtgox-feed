#pragma once

#define COUNTOF(arr) (sizeof(arr) / sizeof(arr[0]))

const std::string CURRENCIES = "EUR,CAD,CHF,SGD,NZD,SEK,NOK,GBP,AUD,JPY,PLN,RUB,CNY,HKD,THB,USD,DKK";

class MtgoxClient
{
    std::mutex _message_sync;
    std::atomic_bool _done;
    std::atomic_int _last_tick_time;
    std::atomic_int _init_time;
    
public:
    MtgoxClient()
    {
        _last_tick_time = time(0);
        _init_time = time(0);
        _done = false;   
    }

    bool GetTicks(FeedData * data)
    {
        PopTicks(data);

        if (data->ticks_count)
        {
            _last_tick_time = time(0);
        }

        #ifdef MTGOX_HTTP
        return !Done() || time(0) - _last_tick_time > 10;
        #else
        return !Done() || time(0) - _last_tick_time > 120;
        #endif
    }

    time_t GetInitTime()
    {
        return _init_time;
    }

protected:
    virtual void PopTicks(FeedData * data) = 0;

    void Done(const bool value)
    {
        _done = value;
    }

    bool Done() const
    {
        return _done;
    }

    boost::property_tree::ptree ConstructPtFromString(const std::string json_string)
    {
        std::lock_guard<std::mutex> lock(_message_sync);

        boost::property_tree::ptree pt;
        std::stringstream json_isstr;
        json_isstr << json_string;

        boost::property_tree::read_json(json_isstr, pt);

        return pt;
    }

    bool PrepareTick(FeedTick & tick, const std::string & symbol, double bid, double ask)
    {
        bid = RoundToDigit(bid, 5);
        ask = RoundToDigit(ask, 5);

        if (bid <= 0 || ask <= 0)
        {
            return false;
        }

        if (bid > ask)
        {
            std::swap(ask, bid);
        }

        tick.bid = bid;
        tick.ask = ask;
        strcpy_s(tick.symbol, symbol.c_str());

        return true;
    }

    static double RoundToDigit(double value, int digit)
    {
        if (digit > 5) 
        {
            digit = 5;
        }

        double point = std::pow(10.0, digit);
        double intpart = 0.0;
        double fractional = std::modf(value, &intpart);

        fractional *= point;
        fractional += value > 0 ? 0.5 : -0.5;
        std::modf(fractional, &fractional);
        fractional /= point;

        return intpart + fractional;
    }

};
