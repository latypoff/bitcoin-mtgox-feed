bitcoin-mtgox-feed
==================

Fast, open-source and easy-to-install Bitcoin price feed for MetaTrader 4.

Getting Started
---------------

-   **Download the latest ready-to-use price feed build here: <https://bitbucket.org/latypoff/bitcoin-mtgox-feed/downloads>**
-   **Download chart history here: <https://bitbucket.org/latypoff/bitcoin-mtgox-feed/downloads>**, request the most up-to-date chart history from <support@latypoff.com>
-   Developers website <http://www.latypoff.com/>
-   Bitbucket source code repository: <https://bitbucket.org/latypoff/bitcoin-mtgox-feed>
-   With all questions please contact <support@latypoff.com>

Supported Currency Pairs
------------------------

The following symbols are currently supported `BTCUSD`, `BTCEUR`, `BTCRUB`, `BTCJPY`, `BTCCAD`, `BTCAUD`, `BTCGBP`, `BTCSGD`,
`BTCNZD`, `BTCSEK`, `BTCNOK`, `BTCCHF`, `BTCPLN`, `BTCCNY`, `BTCHKD`, `BTCTHB`, `BTCDKK`.

Installation Guide
------------------

1.  Download the latest data feed build from [Downloads page](https://bitbucket.org/latypoff/bitcoin-mtgox-feed/downloads) or compile it from sources codes.
2.  Copy `bitcoin-mtgox.feed` to your `/MetaTrader4Server/datafeed` folder
3.  Create a separate group of securities for your convenience.
    
    ![Create securities group](/latypoff/bitcoin-mtgox-feed/raw/master/docs/create-securities.png)
    
4.  Create Bitcoin symbols from the list of supported currency pairs above in your MT4 server configuration (choose those that you would like to use).
    
    ![Create symbols](/latypoff/bitcoin-mtgox-feed/raw/master/docs/create-symbols.png)
    
5.  Configure symbols correctly, sample configuration for BTCUSD is shown on the pictures below:
    
    Set **Contract size** to 1 (which means that 1 lot of BTCUSD is traded as 1 Bitcoin) and specify **Margin calculation** and **Profit calculation** parameters to
    CFD (which ensures that accounts' Forex leverage will not cut down margin of Bitcoin trades).

    ![Symbol/Calculation](/latypoff/bitcoin-mtgox-feed/raw/master/docs/symbol-calculation.png)
    
    Specify BTCUSD name, **Description**, **Type** (set it to a security group created in step 3). We recommend setting **Digits** field to 3, although some symbols
    might be capable of having bigger precision. Also set both **Currency** and **Margin currency** fields to quoted currency of the currency pair (so, for BTCUSD it
    is **USD**, for BTCJPY it is **JPY**)

    ![Symbol/Calculation](/latypoff/bitcoin-mtgox-feed/raw/master/docs/symbol.png)
    
    We suggest you disable **Filtration** and **Swaps** on the symbol.

    BTC is traded 24/7 all over the world, so you might want to allow quotes on the weekends.

    ![Symbol/Calculation](/latypoff/bitcoin-mtgox-feed/raw/master/docs/symbol-sessions.png)
    
4.  Restart your MT4 server
5.  Create a new `bitcoin-mtgox` data feed in MT4 Administrator's *Datafeed* section.
    
    ![Symbol/Calculation](/latypoff/bitcoin-mtgox-feed/raw/master/docs/create-datafeed.png)

    In its options dialog, set the **File:** property to `bitcoin-mtgox.feed` (it should appear there after you've completed steps 2 and 4).

    ![Symbol/Calculation](/latypoff/bitcoin-mtgox-feed/raw/master/docs/datafeed.png)

6.  Now your BTC symbols should be ticking. If MtGox exchange gets offline, the price feed will reconnect automatically.
    
    After the prices are ticking, you can allow demo clients to trade it, or you can set up trading for real clients by using an automated dealer plugin.

7.  In MT4 Administrator's *Charts* section, import the .csv files with chart history downloaded from [Downloads page](https://bitbucket.org/latypoff/bitcoin-mtgox-feed/downloads)

Source code notes
-----------------
1.  The sources depend on Boost 1.54+ libraries — make sure they are installed when you compile. Download them from [Boost C++ Libraries official website](http://www.boost.org/)
2.  **Make sure that you put `FeedInterface.h` file from your MetaTrader 4 developer kit to `/dependencies/mt4/` folder**

